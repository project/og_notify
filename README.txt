
Copyright 2007 Amanuel Tewolde

Description:
------------

This module enables group creators to choose if they want notifications when new registrants join their group.

Installation:
-------------

go to modules and turn it on.
To setup OG Notify please visit the "Group Details" Section of the Organic groups configuration page (http://yoursite.com/admin/og/og) 


Author
------

Amanuel Tewolde
http://ticklespace.com
